# Dockerfile
# Build Stage
FROM node:14 AS build
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

# Production Stage
FROM mysql:8
WORKDIR /app
COPY --from=build /app/dist /app
CMD ["npm", "start"]
